# Contributing

Following these guidelines helps to communicate that you respect the time of
the developers managing and developing this open source project. In return,
they should reciprocate that respect in addressing your issue, assessing
changes, and helping you finalise your pull requests.

## Project Management

Project management is done primarily through [GitLab][1] where issues are used
to track the progress of new features and improvements. The development team is
currently using [Slack][2] for internal communication.

### Branches

- The `master` branch contains the latest stable release.
- The `development` branch is used to stage changes for `master`.

### Versioning

The acchan project and sub-projects use [Semantic Versioning][3].

## Bug Reports

While we adhere to strict software development and quality assurance practices
it's inevitable that software bugs will exist.

There are three project repositories:
- [acchan-client][4]: The Android client.
- [acchan-core][5]: The core engine(s) powering the Android client.
- [acchan-extensions][6]: Extensions for the Android client.

Each of these repositories contain the related subset of the acchan project's
bug reports. If you aren't sure where to file the bug, please lodge the bug
report by [creating an issue][7] in the [acchan-client][4] repository.

## Code Contributions

Before investing time into a new feature, please create an issue and engage
in discussion on how it can be best implemented.

By contributing to the repository you agree that the code contributions will
be licensed under the [bundled license][8].

### Development Environment

The development team uses [Android Studio][9] on various GNU/Linux
distributions. The best support can be provided if you use a similar
development environment.

### Git Commit Style

Please see this [guide on writing good git commit messages][10].

### Code Style

Please refer to the official Android developer [Kotlin style guide][11] and
[Kotlin coding conventions][12].

### Tests

If tests can be written, they must be written.

### Merge Requests

The acchan project uses the [forking workflow][13] with
[fast forward merges][14]. If your merge request addresses an issue, please
mention the issue in your merge request description.

Merge requests must be approved before merging. Trivial merge requests such as
spelling fixes require the approval of at least one maintainer. All other
merge requests require the approval of at least two maintainers.

[1]: https://gitlab.com/acchan-dev
[2]: https://slack.com/
[3]: https://semver.org/
[4]: https://gitlab.com/acchan-dev/acchan-client
[5]: https://gitlab.com/acchan-dev/acchan-core
[6]: https://gitlab.com/acchan-dev/acchan-extensions
[7]: https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html
[8]: https://gitlab.com/acchan-dev/acchan-client/blob/master/LICENSE
[9]: https://developer.android.com/studio/
[10]: https://chris.beams.io/posts/git-commit/
[11]: https://developer.android.com/kotlin/style-guide
[12]: https://kotlinlang.org/docs/reference/coding-conventions.html
[13]: https://docs.gitlab.com/ee/workflow/forking_workflow.html
[14]: https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html
